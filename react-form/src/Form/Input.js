import React, { Component } from "react";

export default class input extends Component {
  state = {
    user: {
      id: "",
      phone: "",
      name: "",
      email: "",
    },
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.svEdited != null) {
      this.setState({ user: nextProps.svEdited });
    }
  }

  handleGetInputFromForm = (e) => {
    let { value, name: key } = e.target;
    let cloneUser = { ...this.state.user };
    cloneUser[key] = value;
    this.setState({ user: cloneUser });
  };

  handleSubmit = () => {
    let newUser = { ...this.state.user };
    this.props.handleAddSinhVien(newUser);
  };

  render() {
    return (
      <div className="text-left">
        <div className="banner bg-info text-light py-2 px-2 mb-4 font-weight-bold">
          Thông tin sinh viên
        </div>
        <div className="form-group row">
          <div className="col-6">
            <label htmlFor="" className="font-weight-bold">
              Mã SV
            </label>
            <input
              value={this.state.user.id}
              onChange={this.handleGetInputFromForm}
              ref={this.inputRef}
              type="text"
              className="form-control mb-3"
              name="id"
              placeholder="Mã sinh viên"
            />
            <label htmlFor="" className="font-weight-bold">
              Số điện thoại
            </label>
            <input
              value={this.state.user.phone}
              onChange={this.handleGetInputFromForm}
              type="text"
              className="form-control"
              name="phone"
              placeholder="Số điện thoại"
            />
          </div>
          <div className="col-6">
            <label htmlFor="" className="font-weight-bold">
              Họ tên
            </label>
            <input
              value={this.state.user.name}
              onChange={this.handleGetInputFromForm}
              type="text"
              className="form-control mb-3"
              name="name"
              placeholder="Họ tên sinh viên"
            />
            <label htmlFor="" className="font-weight-bold">
              Email
            </label>
            <input
              value={this.state.user.email}
              onChange={this.handleGetInputFromForm}
              type="email"
              className="form-control"
              name="email"
              placeholder="Email"
            />
          </div>
        </div>
        <button
          className="btn btn-warning my-4 text-white"
          onClick={this.handleSubmit}
        >
          Thêm sinh viên
        </button>
        <button
          className="btn btn-info my-4 ml-4"
          onClick={() => {
            this.props.handleUpdateSinhVien(
              this.state.user.id,
              this.state.user
            );
          }}
        >
          Cập nhật sinh viên
        </button>
      </div>
    );
  }
}
