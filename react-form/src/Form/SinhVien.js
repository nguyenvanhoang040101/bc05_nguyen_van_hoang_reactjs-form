import React, { Component } from "react";

export default class SinhVien extends Component {
  renderSinhVienTable = () => {
    return this.props.sinhVienList.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.phone}</td>
          <td>{item.email}</td>
          <td>
            <button
              className="btn btn-danger mr-2"
              onClick={() => {
                this.props.handleRemoveSinhVien(item.id);
              }}
            >
              Xóa
            </button>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleEditSinhVien(item, item.id);
              }}
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <div className="form-group w-50 d-flex align-items-center">
          <input
            type="text"
            className="form-control mr-2"
            name="search"
            onChange={this.props.handleSearchInput}
            value={this.props.keyword}
            aria-describedby="helpId"
            placeholder="Tìm kiếm"
          />
          <div className="input-group-prepend">
            <span
              onClick={() => {
                this.props.handleOnSearch(this.props.keyword);
              }}
              className="input-group-text bg-dark"
              style={{ cursor: "pointer", fontSize: 25 }}
            >
              <i className="fa fa-search text-light" />
            </span>
          </div>
        </div>
        <table className="table">
          <thead className="bg-dark text-white">
            <tr>
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderSinhVienTable()}</tbody>
        </table>
      </div>
    );
  }
}
