import React, { Component } from "react";
import Input from "./Input";
import SinhVien from "./SinhVien";
export default class Form extends Component {
  state = {
    sinhVienList: [
      {
        id: 5873,
        name: "Nguyễn Văn Hoàng",
        phone: "19574802010220",
        email: "nguyenvanhoang0415@gmail.com",
      },
      {
        id: 4138,
        name: "Lionel Messi",
        phone: "123456789",
        email: "nguyenvana@fpt.edu.vn",
      },
      {
        id: 6842,
        name: "Barcelona fc",
        phone: "0364892517",
        email: "ayyowazzup@gmail.com",
      },
      {
        id: 1157,
        name: "Nguyễn Văn H",
        phone: "0687915038",
        email: "jdjjdjd@gmail.com",
      },
    ],
    svEdited: null,
    keyword: null,
  };

  handleAddSinhVien = (newSv) => {
    let cloneSinhVienList = [...this.state.sinhVienList, newSv];
    this.setState({ sinhVienList: cloneSinhVienList });
  };

  handleRemoveSinhVien = (userId) => {
    let index = this.state.sinhVienList.findIndex((item) => {
      return item.id === userId;
    });
    if (index !== -1) {
      let cloneSinhVienList = [...this.state.sinhVienList];
      cloneSinhVienList.splice(index, 1);
      this.setState({ sinhVienList: cloneSinhVienList });
    }
  };

  handleEditSinhVien = (value) => {
    this.setState({ svEdited: value });
  };

  handleUpdateSinhVien = (userId, newValue) => {
    let index = this.state.sinhVienList.findIndex((item) => {
      return item.id === userId;
    });
    if (index !== -1) {
      let cloneSinhVienList = [...this.state.sinhVienList];
      cloneSinhVienList[index] = newValue;
      this.setState({ sinhVienList: cloneSinhVienList });
    }
  };

  handleSearchInput = (event) => {
    this.setState({ keyword: event.target.value });
  };

  handleOnSearch = (keyword) => {
    let filterSinhVienList = [...this.state.sinhVienList];
    // console.log(keyword.trim().toLowerCase());
    let renderFilterList = filterSinhVienList.filter(
      (sv) =>
        sv.name.trim().toLowerCase().indexOf(keyword.trim().toLowerCase()) !==
        -1
    );
    //
    // LỖI DÒNG 76, BÁO LỖI FINDINDEX KO PHẢI HÀM
    //
    // if (index !== -1) {
    //   renderFilterList.push(filterSinhVienList[index]);
    // }
    this.setState({ sinhVienList: renderFilterList });
  };

  render() {
    return (
      <div className="py-5 px-5">
        <Input
          svEdited={this.state.svEdited}
          handleAddSinhVien={this.handleAddSinhVien}
          handleUpdateSinhVien={this.handleUpdateSinhVien}
        />
        <SinhVien
          handleRemoveSinhVien={this.handleRemoveSinhVien}
          handleEditSinhVien={this.handleEditSinhVien}
          handleUpdateSinhVien={this.handleUpdateSinhVien}
          handleSearchInput={this.handleSearchInput}
          handleOnSearch={this.handleOnSearch}
          sinhVienList={this.state.sinhVienList}
          keyword={this.state.keyword}
        />
      </div>
    );
  }
}
